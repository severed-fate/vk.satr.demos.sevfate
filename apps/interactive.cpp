#include "interactive.hpp"

#include <Glfw.hpp>
#include <GlfwWindow.hpp>
#include <logger/Logger.hpp>
#include <satr/vk/core/Context.hpp>
#include <satr/vk/core/RenderLoop.hpp>
#include <satr/vk/core/Scene.hpp>

#include <cstdlib>

#include <implot.h>
#include <pthread.h>

/*namespace std {
bool operator<(const std::pair<tcode::common::CommandIndex, std::string_view>& lhs,
               const std::pair<tcode::common::CommandIndex, std::string>& rhs) {
    if (lhs.first < rhs.first) {
        return true;
    }
    else if (rhs.first < lhs.first) {
        return false;
    }
    else if (lhs.second < rhs.second) {
        return true;
    }
    else {
        return false;
    }
}
}  // namespace std*/

eTCodeInteractive::eTCodeInteractive(satr::vk::RenderLoop& root, std::string&& conn_path) :
    satr::vk::DearImguiScene(root), _conn_path(std::move(conn_path)),
    _gm_prov_cfg("/run/media/sima/sque/games/221783_avy/linux_port/data/video_ofs/day1") {}
eTCodeInteractive::~eTCodeInteractive() = default;

size_t eTCodeInteractive::_handle_axes_get_time_delta() {
    auto current_time = get_root().get_loop_time();
    double delta_secs = current_time - _handle_axes_last_time;
    size_t delta_ms = delta_secs * 1000.;
    _handle_axes_last_time += static_cast<double>(delta_ms) / 1000.;
    return delta_ms;
}

bool eTCodeInteractive::on_setup(size_t chain_size, glm::ivec2 scene_size, glm::vec2& offset,
                                 glm::vec2& extent) {
    bool ret = satr::vk::DearImguiScene::on_setup(chain_size, scene_size, offset, extent);
    get_framebuffer().set_color_clear_value(0, BG_COLOR);
    ImPlot::CreateContext();
    return ret;
}

int main(int argc, char* argv[]) {
    pthread_setname_np(pthread_self(), "sevfate");
    logger.configure(true, spec::Logger::Level::Debug, "sevfate.log", spec::Logger::Level::All);
    // Initialization.
    logger.logi("Vk:Satr in startup...");
    satr::nui::GLFW::init_vulkan_loader(vkGetInstanceProcAddr);
    satr::nui::GLFW& glfw = satr::nui::GLFW::get_instance();
    logger.logi("Loaded GLFW, compiled version: ", glfw.get_compiled_version(),
                ", runtime version: ", glfw.get_runtime_version());
    if (!glfw.vulkan_supported()) {
        return EXIT_FAILURE;
    }
    // Window.
    satr::nui::WindowHints window_hints;
    // window_hints.set_maximized(true);
    // window_hints.set_srgb_capable(true);
    window_hints.set_client_api(satr::nui::WindowHints::ClientApi::None);
    satr::nui::Window window = glfw.create_window(
         {640, 480}, "Severed Fate eTCode interactive control", window_hints);
    // Engine setup.
    auto ui_vk_ext = satr::vk::Extensions(glfw);
    // logger.logd("Using GLFW's vulkan extensions: ", ui_vk_ext);
    satr::vk::Context engine("dearimgui", ui_vk_ext);
    satr::vk::DeviceCapabilities::Filter requirements;
    requirements.append_required_extension("VK_KHR_swapchain");
    satr::vk::DeviceCapabilities::Requests features;
    if (!engine.select_device(requirements, features, glfw)) {
        logger.logf_rec("No appropriate vulkan device found!");
        return EXIT_FAILURE;
    }

    satr::vk::RenderLoop compositor(engine, window);
    window.show();
    satr::vk::Scene imgui_scene;
    if (argc > 1) {
        imgui_scene = std::make_shared<eTCodeInteractive>(compositor, std::string(argv[1]));
    }
    else {
        imgui_scene = std::make_shared<eTCodeInteractive>(compositor);
    }
    satr::vk::RenderLoop::SceneProperties imgui_scene_props = {.visibility = 1.0,
                                                               .priority = 1.0};
    compositor.append_scene(imgui_scene, imgui_scene_props);

    compositor.loop();

    return EXIT_SUCCESS;
}
