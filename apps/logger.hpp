#ifndef SATR_VK_SEVFATE_INTERACTIVE_LOGGER_HPP
#define SATR_VK_SEVFATE_INTERACTIVE_LOGGER_HPP
/**
 * @file
 * @brief Proxy header, so it can be easily ported/integrated to other code-bases.
 */

#include <logger/Logger.hpp>

#define SEVFATE_INTERACTIVE_LOGGER_DEBUG(...) logger.logd(__VA_ARGS__)
#define SEVFATE_INTERACTIVE_LOGGER_INFO(...) logger.logi(__VA_ARGS__)
#define SEVFATE_INTERACTIVE_LOGGER_WARN(...) logger.logw(__VA_ARGS__)
#define SEVFATE_INTERACTIVE_LOGGER_ERROR(...) logger.loge(__VA_ARGS__)
#define SEVFATE_INTERACTIVE_LOGGER_FATAL(...) logger.logf(__VA_ARGS__)

#endif /*SATR_VK_SEVFATE_INTERACTIVE_LOGGER_HPP*/
