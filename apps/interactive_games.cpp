#include "interactive.hpp"

#include <game/Config.hpp>
#include <game/ScriptProvider.hpp>
#include <game/Server.hpp>
#include <utils/Trace.hpp>

#include <cstdlib>
#include <memory>

#include <imgui.h>
#include <imgui_stdlib.h>

namespace {

static inline const char* _build_game_tab_compat_service_to_string(
     sevf::game::Compatibility v) {
    switch (v) {
        case sevf::game::Compatibility::Unknown: return "Unknown";
        case sevf::game::Compatibility::FunscriptPlayer: return "FunscriptPlayer";
        case sevf::game::Compatibility::Edi: return "Edi";
        default: std::abort();
    }
}

static inline sevf::game::Compatibility _build_game_tab_compat_service_combo(
     sevf::game::Compatibility v) {
    if (ImGui::BeginCombo("Service type", _build_game_tab_compat_service_to_string(v))) {
        for (int i = 0; i < 3; i++) {
            const bool is_selected = i == static_cast<int>(v);
            if (ImGui::Selectable(_build_game_tab_compat_service_to_string(
                                       static_cast<sevf::game::Compatibility>(i)),
                                  is_selected)) {
                v = static_cast<sevf::game::Compatibility>(i);
            }
            if (is_selected) {
                ImGui::SetItemDefaultFocus();
            }
        }
        ImGui::EndCombo();
    }
    return v;
}

}  // namespace

bool eTCodeInteractive::_init_game_script_server() {
    if (_gm_serv && _gm_serv->is_running()) {
        return false;
    }
    if (_gm_serv == nullptr) {
        _gm_serv = std::make_unique<sevf::game::ScriptServer>();
    }
    if (_gm_serv) {
        bool ok = _gm_serv->setup(_gm_serv_cfg);
        if (ok) {
            _gm_serv->run();
            if (_gm_prov) {
                utils::fatal("eTCodeInteractive::_init_game_script_server: "
                             "logic error, script provider was loaded before server.");
            }
            return true;
        }
    }
    return false;
}
void eTCodeInteractive::_deinit_game_script_server() {
    if (_gm_serv) {
        if (_gm_prov) {
            utils::trace("eTCodeInteractive::_deinit_game_script_server: "
                         "script provider is still initialized!");
            _deinit_game_script_provider();
        }
        _gm_serv->stop();
        _gm_serv.reset();
    }
}
bool eTCodeInteractive::_init_game_script_provider() {
    if (_gm_serv == nullptr) {
        utils::fatal("eTCodeInteractive::_init_game_script_provider: "
                     "script server hasn't been initialized!");
    }
    if (_gm_prov && _gm_prov->is_loaded()) {
        return false;
    }
    if (!_gm_prov) {
        _gm_prov = std::make_shared<sevf::game::ScriptProviderImpl>(_gm_serv->get_ctx());
        if (_gm_prov) {
            // It's not going to move, so just set it once.
            _gm_prov->set_dispatcher(&_state);
            _gm_serv->set_script_provider(_gm_prov);
        }
    }
    if (_gm_prov) {
        bool ok = _gm_prov->setup(_gm_prov_cfg);
        if (ok) {
            return true;
        }
    }
    return false;
}
void eTCodeInteractive::_deinit_game_script_provider() {
    if (_gm_prov) {
        // Clean-up any references being held by axis mapping.
        for (auto& ctl_elem : _axis_control_state) {
            ctl_elem.clear_ctl_game();
        }
        // Un-associate with server.
        if (_gm_serv) {
            _gm_serv->clear_script_provider();
        }
        else {
            utils::fatal("eTCodeInteractive::_deinit_game_script_provider: "
                         "script server was deinit-ed before provider");
        }
        _gm_prov->unload();
        _gm_prov.reset();
    }
}

void eTCodeInteractive::_build_game_tab() {
    bool srv_running = _gm_serv && _gm_serv->is_running();
    bool prov_loaded = _gm_prov && _gm_prov->is_loaded();
    ////////////////////////////////////////////////////////////////////////
    ImGui::SeparatorText("Server");
    ImGui::BeginDisabled(srv_running);
    ImGui::PushID("server");
    {
        auto v = _gm_serv_cfg.compat_service();
        v = _build_game_tab_compat_service_combo(v);
        _gm_serv_cfg.compat_service(v);
    }
    {
        auto v = _gm_serv_cfg.listen_address();
        if (ImGui::InputTextWithHint("Listen address",
                                     sevf::game::ScriptServerConfig::DEFAULT_ADDRESS.c_str(),
                                     &v, ImGuiInputTextFlags_CharsNoBlank)) {
            _gm_serv_cfg.listen_address(v);
        }
    }
    {
        auto v = _gm_serv_cfg.listen_port();
        const char* hint_str;
        switch (_gm_serv_cfg.compat_service()) {
            case sevf::game::Compatibility::Unknown: {
                hint_str = "???";
            } break;
            case sevf::game::Compatibility::FunscriptPlayer: {
                hint_str = sevf::game::ScriptServerConfig::FUNSCRIPTPLAYER_DEFAULT_PORT.c_str();
            } break;
            case sevf::game::Compatibility::Edi: {
                hint_str = sevf::game::ScriptServerConfig::EDI_DEFAULT_PORT.c_str();
            } break;
            default: {
                utils::fatal("Unreachable");
            } break;
        }
        if (ImGui::InputTextWithHint("Listen port", hint_str, &v,
                                     ImGuiInputTextFlags_CharsDecimal)) {
            _gm_serv_cfg.listen_port(v);
        }
    }
    ImGui::PopID();
    ImGui::EndDisabled();

    ////////////////////////////////////////////////////////////////////////
    ImGui::SeparatorText("Provider");
    ImGui::BeginDisabled(prov_loaded);
    ImGui::PushID("provider");
    {
        auto v = _gm_prov_cfg.compat_service();
        v = _build_game_tab_compat_service_combo(v);
        _gm_prov_cfg.compat_service(v);
    }
    {
        std::string v = _gm_prov_cfg.script_path();
        const char* hint_str;
        switch (_gm_prov_cfg.compat_service()) {
            case sevf::game::Compatibility::Unknown: {
                hint_str = "Select service type first";
            } break;
            case sevf::game::Compatibility::FunscriptPlayer: {
                hint_str = "Path to the directory containing the funscripts.";
            } break;
            case sevf::game::Compatibility::Edi: {
                hint_str = "Not implemented";
            } break;
            default: {
                utils::fatal("Unreachable");
            } break;
        }
        if (ImGui::InputTextWithHint("Path", hint_str, &v)) {
            _gm_prov_cfg.script_path(v);
        }
    }
    ImGui::PopID();
    ImGui::EndDisabled();

    ////////////////////////////////////////////////////////////////////////
    ImGui::SeparatorText("State");
    if (srv_running) {
        ImGui::Text("Server running with #%zu connections", _gm_serv->get_connection_count());
    }
    else {
        ImGui::TextUnformatted("Server not running");
    }
    if (prov_loaded) {
        ImGui::Text("Provider loaded with #%zu entries", _gm_prov->get_script_count());
        ImGui::Text("Current script: %s", _gm_prov->get_active_script_name().c_str());
        ImGui::Text("Current playback position: %f", _gm_prov->get_active_script_pos());
        auto state = _gm_prov->get_display_values();
        for (auto [k, v] : state) {
            ImGui::ProgressBar(v, ImVec2(0.0f, 0.0f));
            ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
            ImGui::TextUnformatted(k.c_str());
        }
    }
    else {
        ImGui::TextUnformatted("Provider not loaded");
    }

    ////////////////////////////////////////////////////////////////////////
    ImGui::SeparatorText("Control");
    {
        ImGui::BeginDisabled(prov_loaded);
        if (ImGui::Button(srv_running ? "Stop server" : "Start server")) {
            if (srv_running) {
                _deinit_game_script_server();
            }
            else {
                _init_game_script_server();
            }
        }
        ImGui::EndDisabled();
    }
    ImGui::SameLine();
    {
        ImGui::BeginDisabled(!srv_running);
        if (ImGui::Button(prov_loaded ? "Unload scripts" : "Load scripts")) {
            if (prov_loaded) {
                _deinit_game_script_provider();
            }
            else {
                _init_game_script_provider();
            }
        }
        ImGui::EndDisabled();
    }
    ImGui::SameLine();
    {
        ImGui::BeginDisabled(srv_running != prov_loaded);
        bool state = srv_running || prov_loaded;
        if (ImGui::Button(state ? "De-init both" : "Init both")) {
            if (state) {
                _deinit_game_script_provider();
                _deinit_game_script_server();
            }
            else {
                if (_init_game_script_server()) {
                    _init_game_script_provider();
                }
            }
        }
        ImGui::EndDisabled();
    }
}
