#include "interactive.hpp"
#include <logger.hpp>

#include <implot.h>

void eTCodeInteractive::_on_plot_property_update(tcode::ParserDispatcher&,
                                                 tcode::common::CommandIndex cmd_idx,
                                                 std::string_view prop_name,
                                                 tcode::PropertyMetadata& prop_meta) {
    std::pair lookup_key = {cmd_idx, std::string(prop_name)};
    auto pos = _plot_history.find(lookup_key);
    if (pos == _plot_history.end()) [[unlikely]] {
        SEVFATE_INTERACTIVE_LOGGER_FATAL("_on_plot_property_update: cannot find entry in _plot_history map!");
    }

    const auto& values = prop_meta.get<nlohmann::json>();

    size_t axes_count = prop_meta.get_data_interp_obs_map().y_axes.size() + 1;
    if (!values.is_array() || values.size() != axes_count ||
        std::any_of(values.begin(), values.end(), [](const auto& jv) -> bool {
            return !jv.is_number_float();
        })) [[unlikely]] {
        SEVFATE_INTERACTIVE_LOGGER_ERROR("_on_plot_property_update: received invalid data from device!");
        return;
    }

    for (size_t axis_idx = 0; axis_idx < axes_count; axis_idx++) {
        auto& axis_buffer = pos->second.at(axis_idx);
        const auto& value = values.at(axis_idx);
        float v = value.get<float>();
        axis_buffer.push_front(v);
        if (axis_buffer.size() > PLOT_HISTORY_MAX_BUFFER_SIZE) {
            axis_buffer.pop_back();
        }
    }
}
static ImPlotPoint _build_property_plot_getter(int idx, void* user_data) {
    auto* plot_data =
         reinterpret_cast<std::pair<eTCodeInteractive::plot_history_t*, size_t>*>(user_data);
    auto x = plot_data->first->at(0).at(idx);
    auto y = plot_data->first->at(plot_data->second).at(idx);
    return {x, y};
}
void eTCodeInteractive::_build_property_plot(tcode::common::CommandIndex cmd_idx,
                                             const std::string& prop_name,
                                             tcode::PropertyMetadata& prop_meta) {
    assert(prop_meta.get_type() == tcode::PropertyMetadata::Type::UBJson &&
           prop_meta.is_observation());
    // Implicit parser's registry mutex lock. Also extends to _plot_history.
    auto plot_data_entry = _plot_history.find({cmd_idx, prop_name});
    auto plot_mt = prop_meta.get_data_interp_obs_map();
    if (ImPlot::BeginPlot(prop_name.c_str())) {
        ImPlot::SetupAxes("x", "y", ImPlotAxisFlags_Invert | ImPlotAxisFlags_AutoFit,
                          ImPlotAxisFlags_None);
        size_t n = 0;
        if (plot_data_entry != _plot_history.end() && !plot_data_entry->second.empty()) {
            n = plot_data_entry->second.at(0).size();
        }
        const auto y_n = plot_mt.y_axes.size();
        for (size_t y_i = 0; y_i < y_n; y_i++) {
            std::pair<plot_history_t*, size_t> plot_data = {&plot_data_entry->second, y_i + 1};
            ImPlot::PlotLineG(plot_mt.y_axes.at(y_i).label.c_str(), _build_property_plot_getter,
                              &plot_data, n);
        }
        ImPlot::EndPlot();
    }
}
void eTCodeInteractive::_register_plot_history_callbacks(tcode::Registry& reg) {
    for (auto& cmd_idx_ep : reg.get_endpoints()) {
        tcode::common::CommandIndex cmd_idx = cmd_idx_ep.first;
        tcode::CommandEndpoint& ep = cmd_idx_ep.second;
        for (auto& prop_kv : ep.get_properties()) {
            auto& prop_name = prop_kv.first;
            auto& prop_meta = prop_kv.second;
            if (prop_meta.get_disp_type() == tcode::PropertyMetadata::DisplayType::Plot) {
                // Verify correct device configuration.
                if (prop_meta.get_type() != tcode::PropertyMetadata::Type::UBJson ||
                    !prop_meta.has_flag_event() ||
                    prop_meta.get_data_interp() !=
                         tcode::PropertyMetadata::DataInterpretation::Observations)
                     [[unlikely]] {
                    SEVFATE_INTERACTIVE_LOGGER_ERROR("Invalid configuration for plot property ",
                                cmd_idx.to_null_string(), ".", prop_name, "!");
                    _disconnect();
                }
                // Prepare data log and register callback.
                auto [new_entry, new_entry_ok] = _plot_history.emplace(
                     std::make_pair(cmd_idx, prop_name), plot_history_t{});
                if (!new_entry_ok) {
                    SEVFATE_INTERACTIVE_LOGGER_FATAL("Cannot allocate plot history entry!");
                }
                size_t y_n = prop_meta.get_data_interp_obs_map().y_axes.size();
                new_entry->second = plot_history_t(y_n + 1);
                prop_meta.register_callback(std::bind(
                     &eTCodeInteractive::_on_plot_property_update, this, std::placeholders::_1,
                     std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
            }
        }
    }
}
