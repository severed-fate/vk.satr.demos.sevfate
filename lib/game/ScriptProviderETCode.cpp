#include "ScriptProvider.hpp"

#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>
#include <tcode/Utils.hpp>
#include <utils/Trace.hpp>

namespace sevf::game {

bool ScriptProviderImpl::_dispatch_cmd(tcode::common::CommandIndex cmd_idx) {
    if (cmd_idx.cmd == tcode::common::CommandType::Unknown || cmd_idx.idx < 0) {
        // User hasn't selected an endpoint yet.
        return false;
    }
    tcode::ParserDispatcher* dispatcher;
    {
        std::lock_guard _g(_frontend_mutex);
        dispatcher = _dispatcher;
    }
    if (dispatcher != nullptr && dispatcher->is_connected()) {
        auto [reg_lck, reg] = dispatcher->acquire_registry();
        auto ep_it = reg.get_endpoints().find(cmd_idx);
        if (ep_it != reg.get_endpoints().end()) {
            // TODO: Use immediate requests.
            auto& ep = ep_it->second;
            if (ep.supports_stop_cmd()) {
                ep.pend_stop();
                return true;
            }
            else {
                utils::trace("ScriptProviderImpl::_dispatch_cmd: "
                             "selected endpoint doesn't support normal updates!");
                return false;
            }
        }
        else {
            // Used unsupported cmd_idx.
            utils::fatal("ScriptProviderImpl::_dispatch_cmd: cmd_idx not in registry!");
        }
    }
    else {
        // Ignore all requests.
        return true;
    }
}
bool ScriptProviderImpl::_dispatch_cmd(tcode::common::CommandIndex cmd_idx, float pos) {
    if (cmd_idx.cmd == tcode::common::CommandType::Unknown || cmd_idx.idx < 0) {
        // User hasn't selected an endpoint yet.
        return false;
    }
    tcode::ParserDispatcher* dispatcher;
    {
        std::lock_guard _g(_frontend_mutex);
        dispatcher = _dispatcher;
    }
    if (dispatcher != nullptr && dispatcher->is_connected()) {
        auto [reg_lck, reg] = dispatcher->acquire_registry();
        auto ep_it = reg.get_endpoints().find(cmd_idx);
        if (ep_it != reg.get_endpoints().end()) {
            // TODO: Use immediate requests.
            auto& ep = ep_it->second;

            auto [limit_min, limit_max, reversal] = ep.extract_axis_limits<uint16_t>();
            if (reversal) {
                limit_min = TARGET_DEFAULT;
                limit_max = TARGET_DEFAULT;
            }
            uint32_t scaled_pos =
                 tcode::map(1.f - pos, 0.f, 1.f, (float) limit_min, (float) limit_max);

            utils::trace(cmd_idx, ": pos=", pos, ", scaled_pos=", scaled_pos);

            if (ep.supports_normal_update()) {
                ep.pend_normal_update({scaled_pos, TARGET_MAX});
                return true;
            }
            else {
                utils::trace("ScriptProviderImpl::_dispatch_cmd: "
                             "selected endpoint doesn't support normal updates!");
                return false;
            }
        }
        else {
            // Used unsupported cmd_idx.
            utils::fatal("ScriptProviderImpl::_dispatch_cmd: cmd_idx not in registry!");
        }
    }
    else {
        // Ignore all requests.
        return true;
    }
}
bool ScriptProviderImpl::_dispatch_cmd(tcode::common::CommandIndex cmd_idx, float pos,
                                       uint32_t interval) {
    if (cmd_idx.cmd == tcode::common::CommandType::Unknown || cmd_idx.idx < 0) {
        // User hasn't selected an endpoint yet.
        return false;
    }
    tcode::ParserDispatcher* dispatcher;
    {
        std::lock_guard _g(_frontend_mutex);
        dispatcher = _dispatcher;
    }
    // TODO: is_connected call may have race condition.
    if (dispatcher != nullptr && dispatcher->is_connected()) {
        auto [reg_lck, reg] = dispatcher->acquire_registry();
        auto ep_it = reg.get_endpoints().find(cmd_idx);
        if (ep_it != reg.get_endpoints().end()) {
            // TODO: Use immediate requests.
            auto& ep = ep_it->second;

            auto [limit_min, limit_max, reversal] = ep.extract_axis_limits<uint16_t>();
            if (reversal) {
                limit_min = TARGET_DEFAULT;
                limit_max = TARGET_DEFAULT;
            }
            uint32_t scaled_pos =
                 tcode::map(1.f - pos, 0.f, 1.f, (float) limit_min, (float) limit_max);

            utils::trace(cmd_idx, ": pos=", pos, ", scaled_pos=", scaled_pos,
                         ", interval=", interval);

            if (ep.supports_interval_update()) {
                ep.pend_interval_update({scaled_pos, TARGET_MAX}, interval);
                return true;
            }
            else {
                utils::trace("ScriptProviderImpl::_dispatch_cmd: "
                             "selected endpoint doesn't support interval updates!");
                return false;
            }
        }
        else {
            // Used unsupported cmd_idx.
            utils::fatal("ScriptProviderImpl::_dispatch_cmd: cmd_idx not in registry!");
        }
    }
    else {
        // Ignore all requests.
        return true;
    }
}

}  // namespace sevf::game
