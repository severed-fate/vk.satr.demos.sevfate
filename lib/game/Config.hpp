#ifndef SEVFATE_GAME_CONFIG_HPP
#define SEVFATE_GAME_CONFIG_HPP
/**
 * @file
 * @brief Common configuration structs for game script server and game script provider.
 */

#include <utils/Class.hpp>

#include <filesystem>
#include <string>

namespace sevf::game {

enum class Compatibility {
    Unknown,
    /** https://discuss.eroscripts.com/t/to4st-game-integration-mods/99000 */
    FunscriptPlayer,
    /** https://github.com/NoGRo/Edi */
    Edi,
};

struct ScriptServerConfig {
   public:
    static constexpr std::string FUNSCRIPTPLAYER_DEFAULT_PORT = "5050";
    static constexpr std::string EDI_DEFAULT_PORT = "5000";
    static constexpr std::string DEFAULT_ADDRESS = "127.0.0.1";

   protected:
    Compatibility _compat_service = Compatibility::Unknown;
    std::string _listen_address{};
    std::string _listen_port{};

   public:
    constexpr ScriptServerConfig() noexcept = default;
    constexpr ScriptServerConfig(Compatibility compat_service) noexcept :
        _compat_service(compat_service) {}

    DEFINE_AUTO_VALIDATED_PROPERTY(compat_service, Compatibility::Unknown);
    DEFINE_AUTO_VALIDATED_PROPERTY(listen_address, "");
    DEFINE_AUTO_VALIDATED_PROPERTY(listen_port, "");
};

struct ScriptProviderConfig {
   protected:
    Compatibility _compat_service = Compatibility::Unknown;
    /**
     * Actual directory structure depends on the Compatibility enum.
     */
    std::filesystem::path _script_path = ".";

   public:
    ScriptProviderConfig() noexcept = default;
    ScriptProviderConfig(Compatibility compat_service) noexcept :
        _compat_service(compat_service) {}
    ScriptProviderConfig(std::filesystem::path script_path) noexcept :
        _script_path(script_path) {}

    DEFINE_AUTO_VALIDATED_PROPERTY(compat_service, Compatibility::Unknown);
    DEFINE_AUTO_PROPERTY(script_path);
};

}  // namespace sevf::game

#endif /*SEVFATE_GAME_CONFIG_HPP*/