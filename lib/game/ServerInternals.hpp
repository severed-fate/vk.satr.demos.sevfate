#ifndef SEVFATE_GAME_SERVERINTERNALS_HPP
#define SEVFATE_GAME_SERVERINTERNALS_HPP
/**
 * @file
 * @brief Only available for library internal usage.
 */

#include <asio/buffer.hpp>
#include <asio/ip/tcp.hpp>
#include <utils/Class.hpp>

#include <memory>
#include <set>
#include <unordered_map>
#include <vector>

#include <llhttp.h>

namespace sevf::game {

/** Fwd decl */
class ScriptServer;
class ConnectionManager;

/// Represents a single connection from a client.
class ConnectionImpl : public std::enable_shared_from_this<ConnectionImpl> {
   protected:
    /** Associated connection manager. */
    asio::ip::tcp::socket _socket;
    /** Associated connection manager. */
    ConnectionManager& _conn_manager;
    std::array<char, 3072> _request_buffer;
    llhttp_settings_t _request_parser_settings;
    llhttp_t _request_parser;
    std::string _request_path;
    std::unordered_map<std::string, std::string> _request_query;
    std::string_view _request_last_header_field;
    std::string_view _request_last_header_value;
    std::unordered_map<std::string_view, std::string_view> _request_header_values;
    bool _request_parser_message_complete;

    char _response_buffer[16];

   public:
    /**
     * Construct a connection with the given socket and manager.
     */
    explicit ConnectionImpl(asio::ip::tcp::socket socket, ConnectionManager& manager);

    /**
     * Start the first asynchronous operation for the connection.
     */
    void start();

    /**
     * Stop all asynchronous operations associated with the connection.
     */
    void stop();

   private:
    /**
     * Perform an asynchronous read operation.
     */
    void _pend_read();

    int _request_parser_on_url(const char* at, size_t length);
    static int __request_parser_on_url(llhttp_t* hdl, const char* at, size_t length);
    int _request_parser_on_header_field(const char* at, size_t length);
    static int __request_parser_on_header_field(llhttp_t* hdl, const char* at, size_t length);
    int _request_parser_on_header_value(const char* at, size_t length);
    static int __request_parser_on_header_value(llhttp_t* hdl, const char* at, size_t length);
    int _request_parser_on_header_complete();
    static int __request_parser_on_header_complete(llhttp_t* hdl);
    int _request_parser_on_message_complete();
    static int __request_parser_on_message_complete(llhttp_t* hdl);

    /** For this utility function, the lifetime of the \p phrase must be static. */
    std::vector<asio::const_buffer> _make_simple_response(llhttp_status_t code,
                                                          const char* phrase);

    /**
     * Perform an asynchronous write operation.
     */
    void _do_write(std::vector<asio::const_buffer>&& response_buffers);
};

typedef std::shared_ptr<ConnectionImpl> Connection;

/**
 * Manages open connections.
 */
class ConnectionManager : public meta::INonCopyable {
   protected:
    ScriptServer& _server;
    std::set<Connection> _connections;

   public:
    ConnectionManager(ScriptServer& server) : _server(server) {}

    /** Getters */
    ScriptServer& get_server() const {
        return _server;
    }
    size_t get_connection_count() const {
        return _connections.size();
    }

    /**
     * Add the specified connection to the manager and start it.
     */
    void start(Connection conn);

    /**
     * Stop the specified connection.
     */
    void stop(Connection conn);

    /**
     * Stop all connections.
     */
    void stop_all();
};

}  // namespace sevf::game

#endif /*SEVFATE_GAME_SERVERINTERNALS_HPP*/