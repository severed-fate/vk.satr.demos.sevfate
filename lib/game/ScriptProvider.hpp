#ifndef SEVFATE_GAME_SCRIPTPROVIDER_HPP
#define SEVFATE_GAME_SCRIPTPROVIDER_HPP
/**
 * @file
 * @brief
 */

#include <asio/io_context.hpp>
#include <asio/steady_timer.hpp>
#include <game/Config.hpp>
#include <game/Script.hpp>
#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>
#include <tcode/Utils.hpp>
#include <utils/Class.hpp>

#include <algorithm>
#include <map>
#include <memory>
#include <mutex>
#include <string_view>
#include <unordered_map>
#include <vector>

#include <llhttp.h>

namespace sevf::game {

class ScriptProviderImpl : public meta::INonCopyable,
                           public std::enable_shared_from_this<ScriptProviderImpl> {
   public:
    struct frontend_state_t {
        tcode::common::CommandIndex cmd_idx = {};
        bool invert = false;
        /** Send stop command when no active script is playing. */
        bool stop_on_pause = false;
    };

   protected:
    ScriptProviderConfig _cfg;
    tcode::ParserDispatcher* _dispatcher = nullptr;
    /** For race conditions regarding state accessible by UI thread. */
    std::mutex _frontend_mutex;

    std::unordered_map<std::string, frontend_state_t> _axis_mapping;
    std::map<std::string, std::vector<Funscript>> _scripts;

    std::atomic<std::vector<Funscript>*> _active_script = nullptr;
    std::chrono::time_point<std::chrono::steady_clock> _active_script_start_time;
    asio::steady_timer _active_script_ticker;

    static_assert(std::atomic<void*>::is_always_lock_free);

   public:
    SEVFATE_GAME_API_EXPORT ScriptProviderImpl(asio::io_context& ctx) noexcept;
    ScriptProviderImpl(asio::io_context& ctx, const ScriptProviderConfig& cfg) noexcept :
        ScriptProviderImpl(ctx) {
        assert(setup(cfg));
    }

    /** Getters */
    bool is_loaded() const {
        return !_scripts.empty();
    }
    size_t get_script_count() const {
        return _scripts.size();
    }

    tcode::ParserDispatcher* get_dispatcher() {
        std::lock_guard _g(_frontend_mutex);
        return _dispatcher;
    }

    template<class UnaryFunc> void frontend_for_each_axis_mapping(UnaryFunc f) {
        std::lock_guard _g(_frontend_mutex);
        auto first = _axis_mapping.begin();
        auto last = _axis_mapping.end();
        for (; first != last; ++first) {
            f(&*first);
        }
    }

    // TODO: Re-arrange code so that this can return string views.
    // TODO: Thread safety.
    SEVFATE_GAME_API_EXPORT std::map<std::string, float> get_display_values() const;
    SEVFATE_GAME_API_EXPORT std::string get_active_script_name() const;
    SEVFATE_GAME_API_EXPORT float get_active_script_pos() const;

    /** Setters */
    void set_dispatcher(tcode::ParserDispatcher* dispatcher) {
        std::lock_guard _g(_frontend_mutex);
        _dispatcher = dispatcher;
    }

    SEVFATE_GAME_API_EXPORT bool setup(const ScriptProviderConfig& cfg);
    SEVFATE_GAME_API_EXPORT void unload();

   protected:
    bool _on_request(llhttp_method_t method, const std::string& path,
                     const std::unordered_map<std::string, std::string>& query,
                     const std::unordered_map<std::string_view, std::string_view>& headers);

    frontend_state_t _get_frontend_state_from_script(Funscript&);
    float _get_active_script_play_time() const;
    void _set_active_script_and_schedule(std::vector<Funscript>* active_script);

    friend class ConnectionImpl;

    void _on_tick(bool initial_action = false);

    static constexpr uint32_t TARGET_DIGIT_COUNT = 3;
    static constexpr uint32_t TARGET_MIN = 0;
    static constexpr uint32_t TARGET_MAX = tcode::make_nines<uint32_t, TARGET_DIGIT_COUNT>();
    static constexpr uint32_t TARGET_DEFAULT = (TARGET_MAX + 1) / 2;

    /** Stop command */
    bool _dispatch_cmd(tcode::common::CommandIndex cmd_idx);
    bool _dispatch_cmd(tcode::common::CommandIndex cmd_idx, float pos);
    bool _dispatch_cmd(tcode::common::CommandIndex cmd_idx, float pos, uint32_t interval);
};

typedef std::shared_ptr<ScriptProviderImpl> ScriptProvider;

}  // namespace sevf::game

#endif /*SEVFATE_GAME_SCRIPTPROVIDER_HPP*/