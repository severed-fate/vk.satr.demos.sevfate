#include "Script.hpp"

#include <nlohmann/json.hpp>
#include <utils/Trace.hpp>

#include <cstdlib>
#include <fstream>
#include <limits>
#include <string>

namespace sevf::game {

namespace {
template<typename T> constexpr T __clamp(T v, T mn, T mx) noexcept {
    return (v < mn) ? mn : (v > mx) ? mx : v;
}
}  // namespace

Funscript::Funscript(std::filesystem::path json_path) :
    Funscript(json_path.stem(), nlohmann::json::parse(std::ifstream(json_path))) {}

Funscript::Funscript(std::string name, const nlohmann::json& json_obj) : _title(name) {
    _deserialize(json_obj);
}

std::string Funscript::get_name() const {
    std::filesystem::path pth(_title);
    return pth.stem();
}
std::string Funscript::get_axis() const {
    std::filesystem::path pth(_title);
    std::string str = pth.extension();
    if (!str.empty()) {
        // Remove leading dot('.').
        str = str.substr(1);
    }
    return str;
}

void Funscript::_deserialize(const nlohmann::json& json_obj) {
    if (!json_obj.is_object() || !json_obj["actions"].is_array()) {
        utils::fatal("Failed to load Funscript. No action array found.");
    }

    auto& json_actions = json_obj["actions"];
    _actions.clear();

    for (auto& action : json_actions) {
        float time = action["at"].get<double>() / 1000.0;
        int32_t pos = action["pos"];
        if (time >= 0.f) {
            _actions_emplace(time, __clamp(pos, 0, 100));
        }
    }

    // Use last action as reference for duration.
    if (!_actions.empty()) {
        _duration = _actions.back().atS;
    }

    // TODO: Metadata
}

const FunscriptAction* Funscript::get_action_ahead(float time) const noexcept {
    if (_actions.empty()) {
        return nullptr;
    }
    auto it = _actions_upper_bound(FunscriptAction(time, 0));
    if (it != _actions.end()) {
        return &*it;
    }
    return nullptr;
}
const FunscriptAction* Funscript::get_action_behind(float time) const noexcept {
    if (_actions.empty()) {
        return nullptr;
    }
    auto it = _actions_lower_bound(FunscriptAction(time, 0));
    if (it != _actions.begin()) {
        return &*(--it);
    }
    return nullptr;
}
const FunscriptAction* Funscript::get_action(float time) const noexcept {
    if (_actions.empty()) {
        return nullptr;
    }
    // Gets an action at a time with a margin of error
    float smallest_error = std::numeric_limits<float>::max();
    const FunscriptAction* smallest_error_action = nullptr;

    // Start searching from as close as possible.
    size_t init = 0;
    auto it = _actions_lower_bound(FunscriptAction(time, 0));
    if (it != _actions.end()) {
        init = std::distance(_actions.begin(), it);
        if (init > 0) {
            init -= 1;
        }
    }
    else /* it == _actions.end() */ {
        init = _actions.size() - 2;
        // TODO: Special boundary cases.
    }

    for (size_t i = init; i < std::min(init + 2, _actions.size()); i++) {
        auto& action = _actions[i];

        auto error = time - action.atS;
        auto abs_error = std::abs(error);

        if (abs_error <= smallest_error) {
            smallest_error = error;
            smallest_error_action = &action;
        }
    }

    return smallest_error_action;
}

std::tuple<float, float, float> Funscript::get_interpolated_action(float time) const noexcept {
    if (_actions.size() == 0) {
        return {0, 0, std::numeric_limits<float>::infinity()};
    }
    else if (_actions.size() == 1) {
        float pos_norm = _actions[0].pos / 100.f;
        return {pos_norm, pos_norm, std::numeric_limits<float>::infinity()};
    }

    // Boundary conditions.
    if (time <= _actions.front().atS) {
        float pos_norm = _actions.front().pos / 100.f;
        return {pos_norm, pos_norm, std::numeric_limits<float>::infinity()};
    }
    else if (time >= _actions.back().atS) {
        float pos_norm = _actions.back().pos / 100.f;
        return {pos_norm, pos_norm, std::numeric_limits<float>::infinity()};
    }

    size_t index = 0;
    auto it = _actions_lower_bound(FunscriptAction(time, 0));
    if (it != _actions.end()) {
        index = std::distance(_actions.begin(), it);
        if (index > 0) {
            index -= 1;
        }
    }
    else /* it == _actions.end() */ {
        utils::fatal("Funscript::get_interpolated_action: "
                     "lower_bound returned end.");
    }

    auto& curr_action = _actions[index];
    auto& next_action = _actions[index + 1];

    if (time > curr_action.atS && time < next_action.atS) [[likely]] {
        if (!(curr_action.flags & FunscriptAction::ModeFlagBits::Step)) [[likely]] {
            // Interpolate position.
            float pos_start = curr_action.pos / 100.f;
            float pos_end = next_action.pos / 100.f;
            float pos_diff = pos_end - pos_start;
            float time_diff = next_action.atS - curr_action.atS;
            float factor = (time - curr_action.atS) / time_diff;

            float interp = pos_start + (factor * pos_diff);
            return {interp, pos_end, next_action.atS - time};
        }
        else /* curr_action.flags & FunscriptAction::ModeFlagBits::Step */ {
            // No interpolation for step mode action, just calculate the interval.
            float pos_norm = curr_action.pos / 100.f;
            return {pos_norm, pos_norm, next_action.atS - time};
        }
    }
    else if (curr_action.atS == time) [[unlikely]] {
        utils::fatal("Funscript::get_interpolated_action: "
                     "exact match with curr_action shouldn't be possible.");
        // float pos_norm = curr_action.pos / 100.f;
        // return {pos_norm, pos_norm, 0.};
    }
    else if (next_action.atS == time) {
        float pos_norm = next_action.pos / 100.f;
        return {pos_norm, pos_norm, 0.};
    }
    else {
        utils::fatal("Funscript::get_interpolated_action: "
                     "lower_bound didn't return a valid index.");
    }
}

}  // namespace sevf::game
