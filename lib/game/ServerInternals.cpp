#include "ServerInternals.hpp"

#include <asio/buffer.hpp>
#include <asio/write.hpp>
#include <game/Server.hpp>
#include <utils/Trace.hpp>

#include <cstdio>
#include <cstdlib>
#include <ostream>
#include <string_view>

#include <llhttp.h>

namespace std {

template<typename CTy, typename CTr>
std::basic_ostream<CTy, CTr>& operator<<(std::basic_ostream<CTy, CTr>& o,
                                         const unordered_map<std::string, std::string>& v) {
    for (auto& kv : v) {
        o << kv.first << "=" << kv.second;
    }
    return o;
}

}  // namespace std

namespace sevf::game {

void ConnectionManager::start(Connection conn) {
    _connections.insert(conn);
    conn->start();
}
void ConnectionManager::stop(Connection conn) {
    _connections.erase(conn);
    conn->stop();
}
void ConnectionManager::stop_all() {
    for (auto& conn : _connections) {
        conn->stop();
    }
    _connections.clear();
}

ConnectionImpl::ConnectionImpl(asio::ip::tcp::socket socket, ConnectionManager& manager) :
    _socket(std::move(socket)), _conn_manager(manager),
    _request_parser_message_complete(false) {
    // Initialize http request parser.
    llhttp_settings_init(&_request_parser_settings);
    _request_parser_settings.on_message_complete = __request_parser_on_message_complete;
    _request_parser_settings.on_url = __request_parser_on_url;
    _request_parser_settings.on_header_field = __request_parser_on_header_field;
    _request_parser_settings.on_header_value = __request_parser_on_header_value;
    _request_parser_settings.on_header_value_complete = __request_parser_on_header_complete;
    llhttp_init(&_request_parser, HTTP_REQUEST, &_request_parser_settings);
    _request_parser.data = this;
}
void ConnectionImpl::start() {
    // utils::trace("connection@", this, ": start");
    _pend_read();
}
void ConnectionImpl::stop() {
    // utils::trace("connection@", this, ": close");
    _socket.close();
}

std::vector<asio::const_buffer> ConnectionImpl::_make_simple_response(llhttp_status_t code,
                                                                      const char* phrase) {
    std::vector<asio::const_buffer> buffers;
    buffers.reserve(3);
    {
        auto size =
             std::snprintf(_response_buffer, sizeof(_response_buffer), "HTTP/1.1 %d ", code);
        buffers.push_back(asio::buffer(_response_buffer, size));
    }
    if (phrase != nullptr) {
        std::string_view phrase_view(phrase);
        buffers.push_back(asio::buffer(phrase_view));
    }
    static const char HTTP_DUAL_CRLF[] = {'\r', '\n', '\r', '\n'};
    buffers.push_back(asio::buffer(HTTP_DUAL_CRLF));
    return buffers;
}

void ConnectionImpl::_pend_read() {
    auto self(shared_from_this());
    _socket.async_read_some(
         asio::buffer(_request_buffer),
         [self](std::error_code ec, std::size_t bytes_transferred) -> void {
             if (!ec) {
                 // utils::trace("connection@", self.get(), ": received ", bytes_transferred,
                 //              " bytes.");
                 // Request parsing.
                 llhttp_errno_t err = llhttp_execute(
                      &self->_request_parser, self->_request_buffer.data(), bytes_transferred);
                 if (err == HPE_OK) {
                     if (self->_request_parser_message_complete) {
                         // Request handling.
                         llhttp_method_t request_method =
                              (llhttp_method_t) llhttp_get_method(&self->_request_parser);
                         auto& server = self->_conn_manager.get_server();
                         auto handler = server.get_script_provider();
                         bool ok = false;
                         if (handler) {
                             ok = handler->_on_request(request_method, self->_request_path,
                                                       self->_request_query,
                                                       self->_request_header_values);
                         }
                         // Responding.
                         if (ok) {
                             self->_do_write(self->_make_simple_response(HTTP_STATUS_OK, "OK"));
                         }
                         else {
                             self->_do_write(self->_make_simple_response(
                                  HTTP_STATUS_NOT_FOUND, "ScriptProvider Invalid Request"));
                         }
                     }
                     else {
                         // TODO: Support fragmented requests.
                         // self->_pend_read();
                         self->_do_write(self->_make_simple_response(
                              HTTP_STATUS_INTERNAL_SERVER_ERROR, "Buffer overflow"));
                     }
                 }
                 else {
                     const char* errno_name = llhttp_errno_name(err);
                     utils::trace("llhttp parser error: ", errno_name, "(code: ", (int) err,
                                  ", reason: ", self->_request_parser.reason, ").");
                     self->_do_write(self->_make_simple_response(
                          HTTP_STATUS_INTERNAL_SERVER_ERROR, errno_name));
                 }
             }
             else if (ec != asio::error::operation_aborted) {
                 self->_conn_manager.stop(self);
             }
         });
}

static inline std::string __request_parser_on_url_percent_decode(const std::string_view in) {
    std::string out;
    out.reserve(in.size());

    for (size_t i = 0; i < in.length(); i++) {
        if (in[i] == '%') {
            if (i + 2 < in.length() && isxdigit(in[i + 1]) && isxdigit(in[i + 2])) {
                auto hex_str = std::string(in.substr(i + 1, 2));
                auto hex_val = std::stoi(hex_str, nullptr, 16);
                out.push_back(char(hex_val));
                i += 2;
            }
            else {
                // Malformed % encoding, just append as is
                out.push_back(in[i]);
            }
        }
        else if (in[i] == '+') {
            // '+' is usually decoded to space
            out.push_back(' ');
        }
        else {
            out.push_back(in[i]);
        }
    }

    return out;
}

int ConnectionImpl::_request_parser_on_url(const char* at, size_t length) {
    std::string_view url(at, length);

    // Split to path and query.
    size_t path_end = url.find('?');
    using namespace std::string_view_literals;
    std::string_view path = (path_end == std::string::npos) ? url : url.substr(0, path_end);
    std::string_view query = (path_end == std::string::npos) ? ""sv : url.substr(path_end + 1);

    _request_path = __request_parser_on_url_percent_decode(path);

    size_t query_next_pos;
    do {
        query_next_pos = query.find('&');
        auto subquery = query.substr(0, query_next_pos);
        query = (query_next_pos == std::string::npos) ? ""sv : query.substr(query_next_pos + 1);

        size_t query_split_pos = subquery.find('=');
        if (query_split_pos != std::string::npos) {
            auto key = subquery.substr(0, query_split_pos);
            auto value = subquery.substr(query_split_pos + 1);
            _request_query[__request_parser_on_url_percent_decode(key)] =
                 __request_parser_on_url_percent_decode(value);
        }
    } while (query_next_pos != std::string::npos);

    return 0;
}
int ConnectionImpl::__request_parser_on_url(llhttp_t* hdl, const char* at, size_t length) {
    auto* obj = reinterpret_cast<ConnectionImpl*>(hdl->data);
    return obj->_request_parser_on_url(at, length);
}
int ConnectionImpl::_request_parser_on_header_field(const char* at, size_t length) {
    _request_last_header_field = std::string_view(at, length);
    return 0;
}
int ConnectionImpl::__request_parser_on_header_field(llhttp_t* hdl, const char* at,
                                                     size_t length) {
    auto* obj = reinterpret_cast<ConnectionImpl*>(hdl->data);
    return obj->_request_parser_on_header_field(at, length);
}
int ConnectionImpl::_request_parser_on_header_value(const char* at, size_t length) {
    _request_last_header_value = std::string_view(at, length);
    return 0;
}
int ConnectionImpl::__request_parser_on_header_value(llhttp_t* hdl, const char* at,
                                                     size_t length) {
    auto* obj = reinterpret_cast<ConnectionImpl*>(hdl->data);
    return obj->_request_parser_on_header_value(at, length);
}
int ConnectionImpl::_request_parser_on_header_complete() {
    _request_header_values[_request_last_header_field] = _request_last_header_value;
    return 0;
}
int ConnectionImpl::__request_parser_on_header_complete(llhttp_t* hdl) {
    auto* obj = reinterpret_cast<ConnectionImpl*>(hdl->data);
    return obj->_request_parser_on_header_complete();
}
int ConnectionImpl::_request_parser_on_message_complete() {
    _request_parser_message_complete = true;
    return 0;
}
int ConnectionImpl::__request_parser_on_message_complete(llhttp_t* hdl) {
    auto* obj = reinterpret_cast<ConnectionImpl*>(hdl->data);
    return obj->_request_parser_on_message_complete();
}

void ConnectionImpl::_do_write(std::vector<asio::const_buffer>&& response_buffers) {
    auto self(shared_from_this());
    asio::async_write(
         _socket, std::move(response_buffers),
         [self](std::error_code ec, [[maybe_unused]] std::size_t bytes_transferred) {
             if (!ec) {
                 // utils::trace("connection@", self.get(), ": sent ",
                 //              bytes_transferred, " bytes.");
                 // Initiate graceful connection closure. No keepalive here.
                 asio::error_code ignored_ec;
                 self->_socket.shutdown(asio::ip::tcp::socket::shutdown_both, ignored_ec);
             }

             if (ec != asio::error::operation_aborted) {
                 self->_conn_manager.stop(self);
             }
         });
}

}  // namespace sevf::game
