#ifndef SEVFATE_GAME_SERVER_HPP
#define SEVFATE_GAME_SERVER_HPP
/**
 * @file
 * @brief
 */
#include <asio/io_context.hpp>
#include <asio/ip/tcp.hpp>
#include <game/Common.hpp>
#include <game/Config.hpp>
#include <game/ScriptProvider.hpp>
#include <game/ServerInternals.hpp>
#include <utils/Class.hpp>

#include <cassert>
#include <thread>

#include <unistd.h>

namespace sevf::game {

/**
 * NOTE: Due to some concerns regarding use-after-free in async handlers,
 * it is adviced to construct a new Server object each time.
 * TODO: Signal handlers for safe exit?
 */
class ScriptServer : public meta::INonCopyable {
   protected:
    ScriptServerConfig _cfg;
    ScriptProvider _script_provider;
    /** Asio IO context. Runs on a seperate thread. */
    asio::io_context _conn_ctx;
    /** The worker thread running the asio context. */
    std::thread _conn_thr;

    /** Acceptor used to listen for incoming connections. */
    asio::ip::tcp::acceptor _conn_filter;
    /** 1 connection === 1 client */
    ConnectionManager _conn_manager;

   public:
    SEVFATE_GAME_API_EXPORT ScriptServer() noexcept;
    /**
     * Construct and setup a new server.
     */
    ScriptServer(const ScriptServerConfig& cfg) noexcept : ScriptServer() {
        assert(setup(cfg));
    }
    ~ScriptServer() {
        stop();
    }

    /** Getters */
    SEVFATE_GAME_API_EXPORT bool is_running() const;
    SEVFATE_GAME_API_EXPORT size_t get_connection_count() const;
    auto& get_cfg() const {
        return _cfg;
    };
    auto& get_ctx() {
        return _conn_ctx;
    }
    ScriptProvider get_script_provider() const {
        return _script_provider;
    }

    /** Setters */
    void set_script_provider(ScriptProvider script_provider) {
        _script_provider = script_provider;
    }
    void clear_script_provider() {
        _script_provider.reset();
    }

    /**
     * Configure this server.
     * @returns false if server is running.
     */
    SEVFATE_GAME_API_EXPORT bool setup(const ScriptServerConfig& cfg);

    /**
     * Launch a seperate worker thread and run the server loop.
     * This function returns immediately.
     * @returns false either if server is not setup or if server is already running.
     */
    SEVFATE_GAME_API_EXPORT bool run();

    /**
     * Close all connections and stop the server's worker thread.
     * This function will block until everything has been cleaned-up.
     * This function is a no-op if the server is not running.
     */
    SEVFATE_GAME_API_EXPORT void stop();

   private:
    /** Schedule the handler for accepting incoming connections. */
    void _pend_accept();

    void _conn_thr_main();
};

}  // namespace sevf::game

#endif /*SEVFATE_GAME_SERVER_HPP*/