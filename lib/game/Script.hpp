#ifndef SEVFATE_GAME_SCRIPT_HPP
#define SEVFATE_GAME_SCRIPT_HPP
/**
 * @file
 * @brief Read-only funscript structure. For now it's reduced code from OFS.
 */

#include <game/Common.hpp>
#include <nlohmann/json.hpp>
#include <utils/Class.hpp>
#include <utils/Flags.hpp>

#include <algorithm>
#include <cstdint>
#include <filesystem>
#include <limits>
#include <string_view>
#include <vector>

namespace sevf::game {

struct FunscriptAction {
   public:
    enum class ModeFlagBits : uint8_t {
        Step = 0b0000'0001,
    };
    using ModeFlags = meta::Flags<ModeFlagBits>;

   public:
    // timestamp as floating point seconds
    // instead of integer milliseconds
    float atS;
    int16_t pos;
    ModeFlags flags;
    uint8_t tag;

   public:
    constexpr FunscriptAction() noexcept :
        atS(std::numeric_limits<float>::min()), pos(std::numeric_limits<int16_t>::min()),
        flags{}, tag(0) {}

    constexpr FunscriptAction(float at, int32_t pos) noexcept :
        atS(at), pos(pos), flags{}, tag(0) {}

    constexpr FunscriptAction(float at, int32_t pos, uint8_t tag) noexcept :
        atS(at), pos(pos), flags{}, tag(tag) {}

    constexpr FunscriptAction(float at, int32_t pos, ModeFlags flags) noexcept :
        atS(at), pos(pos), flags(flags), tag(0) {}

    constexpr FunscriptAction(float at, int32_t pos, ModeFlags flags, uint8_t tag) noexcept :
        atS(at), pos(pos), flags(flags), tag(tag) {}

    constexpr bool operator==(FunscriptAction rhs) const noexcept {
        return this->atS == rhs.atS;
    }
    constexpr bool operator!=(FunscriptAction rhs) const noexcept {
        return !(*this == rhs);
    }
    constexpr bool operator<(FunscriptAction rhs) const noexcept {
        return this->atS < rhs.atS;
    }
};

struct FunscriptActionHashfunction {
    inline std::size_t operator()(FunscriptAction s) const noexcept {
        static_assert(sizeof(FunscriptAction) == sizeof(int64_t));
        return *(int64_t*) &s;
    }
};

struct ActionLess {
    constexpr bool operator()(const FunscriptAction& a,
                              const FunscriptAction& b) const noexcept {
        return a.atS < b.atS;
    }
};

class Funscript : public meta::INonCopyable {
   public:
    static constexpr auto EXTENSION = ".funscript";

   protected:
    std::string _title;
    std::vector<FunscriptAction> _actions;
    /** Unlike the duration in funscript's metadata this also includes milliseconds. */
    float _duration = 0;

   public:
    /** Create an empty funscript. */
    constexpr Funscript() noexcept = default;

    /** Load funscript from json path. */
    SEVFATE_GAME_API_EXPORT Funscript(std::filesystem::path json_path);
    /** Load funscript from json object. */
    SEVFATE_GAME_API_EXPORT Funscript(std::string name, const nlohmann::json& json_obj);

    constexpr Funscript(Funscript&&) noexcept = default;
    Funscript& operator=(Funscript&&) noexcept = default;

    /** Getters */
    const auto& get_actions() const {
        return _actions;
    }
    std::string_view get_title() const {
        return _title;
    }
    float get_duration() const {
        return _duration;
    }

    /**
     * Title without axis extension if any.
     */
    SEVFATE_GAME_API_EXPORT std::string get_name() const;
    SEVFATE_GAME_API_EXPORT std::string get_axis() const;

    /** Setters */
    void set_title(std::string_view v) {
        _title = v;
    }
    void set_duration(float v) {
        _duration = v;
    }

    /** Action search */
    SEVFATE_GAME_API_EXPORT const FunscriptAction* get_action_ahead(float time) const noexcept;
    SEVFATE_GAME_API_EXPORT const FunscriptAction* get_action_behind(float time) const noexcept;
    /** Get the closest action */
    SEVFATE_GAME_API_EXPORT const FunscriptAction* get_action(float time) const noexcept;

    /**
     * Calculate and return the current and target position
     * and the remaining time until the target position is reached.
     */
    SEVFATE_GAME_API_EXPORT std::tuple<float, float, float> get_interpolated_action(
         float time) const noexcept;

   protected:
    void _deserialize(const nlohmann::json& json_obj);

    /** vector_set methods */
    void _actions_sort() {
        std::sort(_actions.begin(), _actions.end());
    }

    auto _actions_lower_bound(FunscriptAction act) const {
        return std::lower_bound(_actions.begin(), _actions.end(), act);
    }
    auto _actions_lower_bound(FunscriptAction act) {
        return std::lower_bound(_actions.begin(), _actions.end(), act);
    }

    auto _actions_upper_bound(FunscriptAction act) const {
        return std::upper_bound(_actions.begin(), _actions.end(), act);
    }
    auto _actions_upper_bound(FunscriptAction act) {
        return std::upper_bound(_actions.begin(), _actions.end(), act);
    }

    template<typename... Args> inline bool _actions_emplace(Args&&... args) noexcept {
        using T = decltype(_actions)::value_type;
        T obj(std::forward<Args>(args)...);
        auto it = _actions_lower_bound(obj);

        bool has_equal = false;
        if (it != _actions.end()) {
            has_equal = *it == obj;
        }

        if (!has_equal) {
            _actions.insert(it, std::move(obj));
            return true;
        }
        return false;
    }
};

}  // namespace sevf::game

#endif /*SEVFATE_GAME_SCRIPT_HPP*/
