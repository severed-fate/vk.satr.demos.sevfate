#ifndef SEVFATE_GAME_COMMON_HPP
#define SEVFATE_GAME_COMMON_HPP
/**
 * @file
 * @brief 
 */

#ifdef SEVFATE_GAME_BUILD
    #define SEVFATE_GAME_API_EXPORT __attribute__((visibility("default")))
#else
    #define SEVFATE_GAME_API_EXPORT
#endif

#endif /*SEVFATE_GAME_COMMON_HPP*/