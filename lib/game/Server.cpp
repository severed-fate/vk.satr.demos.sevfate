#include "Server.hpp"

#include <game/Config.hpp>
#include <game/ServerInternals.hpp>
#include <utils/Trace.hpp>

namespace sevf::game {

ScriptServer::ScriptServer() noexcept :
    _cfg(), _conn_ctx(1), _conn_filter(_conn_ctx), _conn_manager(*this) {
    // The server is by default in 'stopped' state.
    _conn_ctx.stop();
}

bool ScriptServer::is_running() const {
    return !_conn_ctx.stopped();
}
size_t ScriptServer::get_connection_count() const {
    return _conn_manager.get_connection_count();
}

bool ScriptServer::setup(const ScriptServerConfig& cfg) {
    if (is_running()) [[unlikely]] {
        utils::trace("ScriptServer::setup: is_running");
        return false;
    }

    // Parse configuration.
    _cfg = cfg;
    if (!_cfg.has_compat_service()) [[unlikely]] {
        utils::trace("ScriptServer::setup: not cfg.has_compat_service");
        return false;
    }
    if (!_cfg.has_listen_address()) {
        _cfg.listen_address(ScriptServerConfig::DEFAULT_ADDRESS);
    }
    if (!_cfg.has_listen_port()) {
        switch (_cfg.compat_service()) {
            case Compatibility::FunscriptPlayer: {
                _cfg.listen_port(ScriptServerConfig::FUNSCRIPTPLAYER_DEFAULT_PORT);
            } break;
            case Compatibility::Edi: {
                _cfg.listen_port(ScriptServerConfig::EDI_DEFAULT_PORT);
            } break;
            default: {
                utils::fatal("ScriptServer::setup: Unknown Compatibility enum");
            } break;
        }
    }

    utils::trace("ScriptServer: listening on ", _cfg.listen_address(), ':', _cfg.listen_port());

    /**
     * Open the acceptor with the option to reuse the address.
     * This is mostly to work around the sockets in TIME_WAIT state.
     */
    asio::ip::tcp::resolver resolver(_conn_ctx);
    asio::ip::tcp::endpoint endpoint =
         *resolver.resolve(_cfg.listen_address(), _cfg.listen_port()).begin();
    _conn_filter.open(endpoint.protocol());
    _conn_filter.set_option(asio::ip::tcp::acceptor::reuse_address(true));
    _conn_filter.bind(endpoint);
    _conn_filter.listen();

    _pend_accept();

    return true;
}

bool ScriptServer::run() {
    if (is_running()) [[unlikely]] {
        utils::trace("ScriptServer::run: is_running");
        return false;
    }
    // Verify configuration.
    if (!_cfg.has_compat_service()) [[unlikely]] {
        utils::trace("ScriptServer::run: not cfg.has_compat_service");
        return false;
    }
    // IO handler thread is still running, but context is stopped!
    if (_conn_thr.get_id() != std::thread::id()) [[unlikely]] {
        utils::trace("ScriptServer::run: _conn_thr not empty");
        return false;
    }

    _conn_ctx.restart();
    _conn_thr = std::thread(&ScriptServer::_conn_thr_main, this);

    return true;
}
void ScriptServer::_conn_thr_main() {
    {
        std::stringstream name("game_io_thread@");
        name << (const void*) this;
        pthread_setname_np(pthread_self(), name.str().c_str());
        utils::trace("ScriptServer handler thread starting...");
    }

    // asio::error_code ec;
    while (!_conn_ctx.stopped()) {
        _conn_ctx.run();
        // _conn_ctx.run(ec);
        // if (ec) {
        //     utils::trace("ScriptServer::_conn_thr_main: Error while handling io events: ",
        //                  ec.message(), " (", ec, ").");
        // }
    }

    utils::trace("ScriptServer handler thread exiting...");
}
void ScriptServer::stop() {
    if (is_running()) [[likely]] {
        _conn_filter.close();
        _conn_manager.stop_all();
    }
    _conn_ctx.stop();
    if (_conn_thr.get_id() != std::thread::id()) {
        _conn_thr.join();
    }
}

void ScriptServer::_pend_accept() {
    _conn_filter.async_accept([this](std::error_code ec, asio::ip::tcp::socket socket) {
        // Check whether the socket was closed before this handler had a chance to run.
        if (!_conn_filter.is_open()) [[unlikely]] {
            return;
        }

        if (!ec) {
            auto conn = std::make_shared<ConnectionImpl>(std::move(socket), _conn_manager);
            _conn_manager.start(conn);
        }

        // Schedule next.
        _pend_accept();
    });
}

}  // namespace sevf::game
