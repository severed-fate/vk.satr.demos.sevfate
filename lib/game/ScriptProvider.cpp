#include "ScriptProvider.hpp"
#include "utils/Trace.hpp"

#include <game/Script.hpp>
#include <tcode/Messages.hpp>

#include <chrono>
#include <filesystem>
#include <mutex>
#include <vector>

namespace sevf::game {

ScriptProviderImpl::ScriptProviderImpl(asio::io_context& ctx) noexcept :
    _active_script_ticker(ctx) {}

bool ScriptProviderImpl::setup(const ScriptProviderConfig& cfg) {
    if (is_loaded()) [[unlikely]] {
        utils::trace("ScriptProvider::setup: is_loaded");
        return false;
    }

    // Parse configuration.
    _cfg = cfg;
    if (!_cfg.has_compat_service()) [[unlikely]] {
        utils::trace("ScriptProvider::setup: not cfg.has_compat_service");
        return false;
    }
    if (!std::filesystem::is_directory(_cfg.script_path())) {
        utils::trace("ScriptProvider::setup: cfg.script_path not a directory");
        return false;
    }

    switch (_cfg.compat_service()) {
        case Compatibility::FunscriptPlayer: {
            // Load scripts one by one.
            for (const auto& entry : std::filesystem::directory_iterator(
                      _cfg.script_path(),
                      std::filesystem::directory_options::follow_directory_symlink)) {
                if (!entry.is_regular_file()) {
                    // Skip everything that isn't a file.
                    continue;
                }
                const auto& entry_path = entry.path();
                if (entry_path.extension() == Funscript::EXTENSION) {
                    Funscript script(entry_path);
                    auto axis_name = script.get_axis();
                    auto base_name = script.get_name();
                    {
                        std::lock_guard _g(_frontend_mutex);
                        if (!_axis_mapping.contains(axis_name)) {
                            _axis_mapping.insert({axis_name, frontend_state_t()});
                        }
                    }
                    if (!_scripts.contains(base_name)) {
                        _scripts[base_name] = std::vector<Funscript>();
                    }
                    _scripts.at(base_name).push_back(std::move(script));
                }
            }
        } break;
        default: {
            utils::trace("ScriptProvider::setup: unimplemented cfg.compat_service");
            return false;
        } break;
    }

    return !_scripts.empty();
}
void ScriptProviderImpl::unload() {
    _active_script_ticker.cancel();
    _active_script = nullptr;
    _scripts.clear();
    {
        std::lock_guard _g(_frontend_mutex);
        _axis_mapping.clear();
    }
}

bool ScriptProviderImpl::_on_request(
     llhttp_method_t method, const std::string& path,
     const std::unordered_map<std::string, std::string>& query,
     [[maybe_unused]] const std::unordered_map<std::string_view, std::string_view>& headers) {
    if (!is_loaded()) {
        return false;
    }

    switch (_cfg.compat_service()) {
        case Compatibility::FunscriptPlayer: {
            if (method == HTTP_GET && path == "/game/gallery") {
                if (query.empty()) {
                    // Stop.
                    _set_active_script_and_schedule(nullptr);
                    return true;
                }
                else if (query.contains("code")) {
                    const std::string& query_code = query.at("code");
                    // utils::trace("received play request for ", query_code);
                    auto scriptset_it = _scripts.find(query_code);
                    if (scriptset_it != _scripts.end()) {
                        _set_active_script_and_schedule(&scriptset_it->second);
                    }
                    else {
                        // utils::trace("request's script not found!");
                        _set_active_script_and_schedule(nullptr);
                    }
                    return true;
                }
            }
            return false;
        } break;
        default: {
            utils::trace("ScriptProvider::_on_request: unimplemented cfg.compat_service");
            return false;
        } break;
    }
}

ScriptProviderImpl::frontend_state_t ScriptProviderImpl::_get_frontend_state_from_script(
     Funscript& script) {
    auto axis_name = script.get_axis();
    std::lock_guard _g(_frontend_mutex);
    return _axis_mapping.at(axis_name);
}

float ScriptProviderImpl::_get_active_script_play_time() const {
    if (_active_script == nullptr) {
        return 0;
    }
    auto current_time = std::chrono::steady_clock::now();
    auto diff_time = current_time - _active_script_start_time;
    std::chrono::duration<double> diff_time_secs = diff_time;
    return diff_time_secs.count();
}

void ScriptProviderImpl::_set_active_script_and_schedule(
     std::vector<Funscript>* active_script) {
    if (active_script == nullptr) {
        if (_active_script != nullptr) {
            // Stop at current position.
            for (auto& script : *_active_script) {
                auto uistate = _get_frontend_state_from_script(script);
                if (uistate.stop_on_pause) {
                    _dispatch_cmd(uistate.cmd_idx);
                }
                else {
                    auto [pos, target, interval] =
                         script.get_interpolated_action(_get_active_script_play_time());
                    // if (uistate.invert) {
                    //     pos = 1.f - pos;
                    //     target = 1.f - pos;
                    // }
                    _dispatch_cmd(uistate.cmd_idx, pos);
                }
            }
        }
        _active_script_ticker.cancel();
        _active_script = nullptr;
    }
    else {
        // Start new script.
        _active_script_start_time = std::chrono::steady_clock::now();
        _active_script = active_script;
        // Send initial command.
        _on_tick(true);
    }
}

void ScriptProviderImpl::_on_tick(bool initial_action) {
    if (_active_script != nullptr) {
        float time = _get_active_script_play_time();
        float tick_interval = initial_action ? 1. / 30. : 1. / 3.;
        for (auto& script : *_active_script) {
            auto uistate = _get_frontend_state_from_script(script);
            auto [pos, target, interval] = script.get_interpolated_action(time);
            if (uistate.invert) {
                pos -= 1.f - pos;
                target = 1.f - pos;
                // utils::trace("inverting!!!!!!!");
            }else {
                // utils::trace("not inverting########");
            }
            if (initial_action || interval <= 10.f / 1000.f) {
                _dispatch_cmd(uistate.cmd_idx, pos);
            }
            else {
                _dispatch_cmd(uistate.cmd_idx, target, interval * 1000.f);
            }
            tick_interval = std::min(tick_interval, interval);
        }
        // Force a minimum interval between ticks of 10 ms.
        tick_interval = std::max(tick_interval, 10.f / 1000.f);
        std::chrono::duration<float> tick_interval_fp_secs(tick_interval);
        auto tick_interval_dur =
             std::chrono::duration_cast<std::chrono::steady_clock::duration>(
                  tick_interval_fp_secs);
        _active_script_ticker.expires_after(tick_interval_dur);
    }

    // Schedule next tick.
    auto self(shared_from_this());
    _active_script_ticker.async_wait([self](const asio::error_code& e) {
        if (e != asio::error::operation_aborted) {
            self->_on_tick();
        }
    });
}

std::string ScriptProviderImpl::get_active_script_name() const {
    auto* active_script = _active_script.load();
    if (active_script != nullptr && !active_script->empty()) {
        active_script->front().get_name();
    }
    return "";
}
SEVFATE_GAME_API_EXPORT float ScriptProviderImpl::get_active_script_pos() const {
    return _get_active_script_play_time();
}

std::map<std::string, float> ScriptProviderImpl::get_display_values() const {
    auto* active_script = _active_script.load();
    if (active_script != nullptr) {
        float time = _get_active_script_play_time();
        std::map<std::string, float> ret;
        for (auto& script : *active_script) {
            auto [pos, target, interval] = script.get_interpolated_action(time);
            ret.insert({script.get_axis(), pos});
        }
        return ret;
    }
    return {};
}

}  // namespace sevf::game
