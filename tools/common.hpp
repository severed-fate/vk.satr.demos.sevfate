#ifndef SATR_VK_SEVFATE_TOOLS_COMMON_HPP
#define SATR_VK_SEVFATE_TOOLS_COMMON_HPP
/**
 * @file
 * @brief Argument parsing for all of sevfate tools.
 */

#include <tcode/ParserDispatcher.hpp>

#include <string>

#include <exit_codes.h>

namespace sevfate {

bool parse_arguments(const std::string& program_name, const std::string& program_version, const std::string& program_help, int argc,
                     char* argv[], std::string& dev_path, tcode::ConnectionConfig& dev_cfg);

}

#endif /*SATR_VK_SEVFATE_TOOLS_COMMON_HPP*/