#include "common.hpp"
#include "exit_codes.h"

#include <logger/Logger.hpp>
#include <stopwatch/StopWatch.hpp>
#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>

#include <mutex>

/**
 * The measurement methodology is imperfect, but should be representative of real world usage.
 */

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    tcode::ParserDispatcher tcode_protocol;
    {
        std::string dev_path = "/dev/ttyACM0";
        tcode::ConnectionConfig dev_cfg = {};
        if (!sevfate::parse_arguments("bandwidth", "0.7",
                                      "Measure bandwidth of connection to the device.", argc,
                                      argv, dev_path, dev_cfg)) {
            return TEST_ERROR;
        }
        tcode_protocol.connect(dev_path, dev_cfg);
    }

    if (!tcode_protocol.is_connected()) {
        return TEST_FAIL;
    }

    spec::StopWatch watch;
    std::mutex watch_mutex;

    tcode_protocol.register_on_response_end_callback([&](tcode::ParserDispatcher& obj) -> bool {
        {
            std::lock_guard _g(watch_mutex);
            watch.stop();
        }
        obj.begin_request();
        // std::this_thread::sleep_for(std::chrono::milliseconds(10));
        obj.end_request();
        {
            std::lock_guard _g(watch_mutex);
            watch.start();
        }
        return true;
    });

    tcode_protocol.start_detached_event_loop([&](tcode::ParserDispatcher& obj) {
        obj.begin_request();
        obj.end_request();
        {
            std::lock_guard _g(watch_mutex);
            watch.start();
        }
    });

    // Keep object 'tcode_protocol' in scope and print stats periodically.
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        {
            std::lock_guard _g(watch_mutex);
            logger.logi("rtt min/avg/max = ", watch.get_min_time(), '/', watch.get_avg_time(),
                        '/', watch.get_max_time(), " ms");
            watch.reset();
        }
    } while (tcode_protocol.is_connected());

    return TEST_OK;
}