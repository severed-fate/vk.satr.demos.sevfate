#include "common.hpp"
#include "exit_codes.h"

#include <logger/Logger.hpp>
#include <stopwatch/StopWatch.hpp>
#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>

#include <thread>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    tcode::ParserDispatcher tcode_protocol;
    {
        std::string dev_path = "/dev/ttyACM0";
        tcode::ConnectionConfig dev_cfg = {};
        if (!sevfate::parse_arguments("bandwidth", "0.7",
                                      "Measure bandwidth of connection to the device.", argc,
                                      argv, dev_path, dev_cfg)) {
            return TEST_ERROR;
        }
        tcode_protocol.connect(dev_path, dev_cfg);
    }

    if (!tcode_protocol.is_connected()) {
        return TEST_FAIL;
    }

    tcode_protocol.set_packet_tracing(true);

    tcode_protocol.begin_request();
    tcode_protocol.send_request({tcode::request::CommandType::Device, 0});
    tcode_protocol.send_request({tcode::request::CommandType::Device, 1});
    tcode_protocol.send_request({tcode::request::CommandType::Device, 2});
    tcode_protocol.end_request();

    spec::StopWatch sw_d0name;
    spec::StopWatch sw_d0version;
    spec::StopWatch sw_d1name;

    tcode_protocol.register_on_response_end_callback([&](tcode::ParserDispatcher& obj) -> bool {
        if (obj.is_response_pending()) {
            return true;
        }

        auto [reg_lck, reg] = obj.acquire_registry();

        if (reg.get_endpoints().size() == 0) {
            // Still waiting for enumeration info.
            return true;
        }

        // Register property update callbacks.
        auto& endpts = reg.get_endpoints();
        auto& endpt_d0 = endpts.at({tcode::common::CommandType::Device, 0});
        auto& endpt_d1 = endpts.at({tcode::common::CommandType::Device, 1});

        endpt_d0.get_properties().at("name").register_callback(
             [&](tcode::ParserDispatcher&, tcode::common::CommandIndex,
                 [[maybe_unused]] std::string_view prop_name,
                 [[maybe_unused]] tcode::PropertyMetadata& prop_mtdt) {
                 sw_d0name.stop();
                 sw_d0name.start();
             });
        endpt_d1.get_properties().at("name").register_callback(
             [&](tcode::ParserDispatcher&, tcode::common::CommandIndex,
                 [[maybe_unused]] std::string_view prop_name,
                 [[maybe_unused]] tcode::PropertyMetadata& prop_mtdt) {
                 sw_d1name.stop();
                 sw_d1name.start();
             });
        endpt_d0.get_properties().at("version").register_callback(
             [&](tcode::ParserDispatcher&, tcode::common::CommandIndex,
                 [[maybe_unused]] std::string_view prop_name,
                 [[maybe_unused]] tcode::PropertyMetadata& prop_mtdt) {
                 sw_d0version.stop();
                 sw_d0version.start();
             });

        sw_d0name.start();
        sw_d0version.start();
        sw_d1name.start();

        // Assign update intervals.
        obj.begin_request();
        obj.send_request({tcode::common::CommandType::Device, 0}, "name",
                         tcode::request::IntervalData(350));
        obj.send_request({tcode::common::CommandType::Device, 1}, "name",
                         tcode::request::IntervalData(460));
        obj.send_request({tcode::common::CommandType::Device, 0}, "version",
                         tcode::request::IntervalData(130));
        obj.end_request();
        // Remove setup callback (this lambda).
        tcode_protocol.register_on_response_end_callback({});
        return true;
    });

    tcode_protocol.start_detached_event_loop();
    std::this_thread::sleep_for(std::chrono::milliseconds(9000 * 5));

    tcode_protocol.begin_request();
    tcode_protocol.send_request({tcode::common::CommandType::Device, 0}, "name",
                                tcode::request::IntervalData(0));
    tcode_protocol.send_request({tcode::common::CommandType::Device, 1}, "name",
                                tcode::request::IntervalData(0));
    tcode_protocol.send_request({tcode::common::CommandType::Device, 0}, "version",
                                tcode::request::IntervalData(0));
    tcode_protocol.end_request();

    // Print collected timing statistics.
    logger.logi("D0 name min/avg/max = ", sw_d0name.get_min_time(), '/',
                sw_d0name.get_avg_time(), '/', sw_d0name.get_max_time(), " ms");

    logger.logi("D1 name min/avg/max = ", sw_d1name.get_min_time(), '/',
                sw_d1name.get_avg_time(), '/', sw_d1name.get_max_time(), " ms");

    logger.logi("D0 version min/avg/max = ", sw_d0version.get_min_time(), '/',
                sw_d0version.get_avg_time(), '/', sw_d0version.get_max_time(), " ms");

    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    return TEST_OK;
}