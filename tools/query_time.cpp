#include "common.hpp"
#include "exit_codes.h"

#include <logger/Logger.hpp>
#include <stopwatch/StopWatch.hpp>
#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>
#include <tcode/Utils.hpp>

#include <chrono>
#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    tcode::ParserDispatcher tcode_protocol;
    {
        std::string dev_path = "/dev/ttyACM0";
        tcode::ConnectionConfig dev_cfg = {};
        if (!sevfate::parse_arguments("bandwidth", "0.7",
                                      "Measure bandwidth of connection to the device.", argc,
                                      argv, dev_path, dev_cfg)) {
            return TEST_ERROR;
        }
        tcode_protocol.connect(dev_path, dev_cfg);
    }

    if (!tcode_protocol.is_connected()) {
        return TEST_FAIL;
    }

    tcode_protocol.start_detached_event_loop([&](tcode::ParserDispatcher& obj) {
        obj.begin_request();
        obj.send_request({tcode::request::CommandType::Device, 0});
        obj.send_request({tcode::request::CommandType::Device, 1});
        obj.send_request({tcode::request::CommandType::Device, 2});
        obj.end_request();
    });

    // Wait for enumeration completion.
    while (tcode_protocol.is_response_pending() ||
           tcode_protocol.acquire_registry().second.get_endpoints().size() == 0) {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));
    }

    // Register update callbacks.
    {
        auto [reg_lck, reg] = tcode_protocol.acquire_registry();
        auto& rtos_time_prop = reg.get_endpoints()
                                    .at({tcode::request::CommandType::Device, 4})
                                    .get_properties()
                                    .at("time");
        rtos_time_prop.register_callback([](tcode::ParserDispatcher&,
                                            tcode::common::CommandIndex, std::string_view,
                                            tcode::PropertyMetadata& prop_data) {
            auto device_time = prop_data.get<int64_t>();
            auto current_utc_time = std::chrono::utc_clock::now();
            int64_t current_utc_millis = std::chrono::duration_cast<std::chrono::milliseconds>(
                                              current_utc_time.time_since_epoch())
                                              .count();
            std::cout << "Device time: " << device_time
                      << " (error: " << current_utc_millis - device_time << ")" << std::endl;
        });
    }

    {
        // Request periodic updates.
        tcode_protocol.begin_request();
        tcode_protocol.send_request({tcode::request::CommandType::Device, 4}, "time",
                                    tcode::request::IntervalData(1000));
        tcode_protocol.end_request();
    }

    // Keep object 'tcode_protocol' in scope to print stats periodically.
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(333));
    } while (tcode_protocol.is_connected());

    return TEST_OK;
}
