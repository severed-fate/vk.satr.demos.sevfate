#include "exit_codes.h"

#include <game/Config.hpp>
#include <game/Server.hpp>
#include <logger/Logger.hpp>

#include <chrono>
#include <thread>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    static constexpr sevf::game::ScriptServerConfig cfg(
         sevf::game::Compatibility::FunscriptPlayer);
    sevf::game::ScriptServer server;
    if (!server.setup(cfg)) {
        logger.loge("Cannot setup server!");
        return TEST_ERROR;
    }
    if (!server.run()) {
        logger.loge("Cannot run server!");
        return TEST_ERROR;
    }

    std::this_thread::sleep_for(std::chrono::seconds(20));

    server.stop();

    return TEST_OK;
}
