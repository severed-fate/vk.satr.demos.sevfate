#include "common.hpp"
#include "exit_codes.h"

#include <logger/Logger.hpp>
#include <tcode/ParserDispatcher.hpp>

#include <thread>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    tcode::ParserDispatcher tcode_protocol;
    // Parse connection options.
    std::string dev_path = "/dev/ttyACM0";
    tcode::ConnectionConfig dev_cfg = {};
    if (!sevfate::parse_arguments("bandwidth", "0.7",
                                  "Measure bandwidth of connection to the device.", argc, argv,
                                  dev_path, dev_cfg)) {
        return TEST_ERROR;
    }

    int disconnect_counter = 0;

    while (disconnect_counter <= 60) {
        // Start connection.
        tcode_protocol.connect(dev_path, dev_cfg);
        // Still connecting.
        while (!tcode_protocol.is_connected() && tcode_protocol.is_connecting()) {
            logger.logi("Connecting...");
            tcode_protocol.poll_events();
            std::this_thread::sleep_for(std::chrono::milliseconds(300));
        }
        // Send one ping if connected.
        if (tcode_protocol.is_connected()) {
            logger.logi("Connected.");
            disconnect_counter = 0;
            tcode_protocol.begin_request();
            tcode_protocol.end_request();
        }
        // Simulate event loop.
        while (tcode_protocol.is_connected()) {
            logger.logd("polling: ", tcode_protocol.poll_events());
            std::this_thread::sleep_for(std::chrono::milliseconds(300));
        }
        logger.logi("Disconnected.");
        disconnect_counter++;
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    return TEST_OK;
}