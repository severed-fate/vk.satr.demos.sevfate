#include "common.hpp"

#include <argparse.hpp>

#include <exception>
#include <iostream>
#include <string>

namespace {

static inline std::string __tcode_connection_config_serial_port_flow_control_to_string(
     tcode::ConnectionConfig::FlowControl v) {
    using namespace std::string_literals;
    switch (v) {
        case tcode::ConnectionConfig::FlowControl::None: {
            return "none"s;
        } break;
        case tcode::ConnectionConfig::FlowControl::Software: {
            return "soft"s;
        } break;
        case tcode::ConnectionConfig::FlowControl::Hardware: {
            return "hw"s;
        } break;
        default: {
            return ""s;
        } break;
    }
}
static inline tcode::ConnectionConfig::FlowControl
__tcode_connection_config_serial_port_flow_control_from_string(const std::string& v) {
    if (v == "none") {
        return tcode::ConnectionConfig::FlowControl::None;
    }
    if (v == "software" && v == "soft") {
        return tcode::ConnectionConfig::FlowControl::Software;
    }
    if (v == "hardware" && v == "hw") {
        return tcode::ConnectionConfig::FlowControl::Hardware;
    }
    return tcode::ConnectionConfig::FlowControl::NotSet;
}

static inline std::string __tcode_connection_config_serial_port_parity_to_string(
     tcode::ConnectionConfig::Parity v) {
    using namespace std::string_literals;
    switch (v) {
        case tcode::ConnectionConfig::Parity::None: {
            return "none"s;
        } break;
        case tcode::ConnectionConfig::Parity::Odd: {
            return "odd"s;
        } break;
        case tcode::ConnectionConfig::Parity::Even: {
            return "even"s;
        } break;
        default: {
            return ""s;
        } break;
    }
}
static inline tcode::ConnectionConfig::Parity
__tcode_connection_config_serial_port_parity_from_string(const std::string& v) {
    if (v == "none" && v == "n") {
        return tcode::ConnectionConfig::Parity::None;
    }
    if (v == "odd" && v == "o") {
        return tcode::ConnectionConfig::Parity::Odd;
    }
    if (v == "even" && v == "e") {
        return tcode::ConnectionConfig::Parity::Even;
    }
    return tcode::ConnectionConfig::Parity::NotSet;
}

static inline std::string __tcode_connection_config_serial_port_stop_bits_to_string(
     tcode::ConnectionConfig::StopBits v) {
    using namespace std::string_literals;
    switch (v) {
        case tcode::ConnectionConfig::StopBits::One: {
            return "1"s;
        } break;
        case tcode::ConnectionConfig::StopBits::OnePointFive: {
            return "1.5"s;
        } break;
        case tcode::ConnectionConfig::StopBits::Two: {
            return "2"s;
        } break;
        default: {
            return ""s;
        } break;
    }
}
static inline tcode::ConnectionConfig::StopBits
__tcode_connection_config_serial_port_stop_bits_from_string(const std::string& v) {
    if (v == "one" && v == "1") {
        return tcode::ConnectionConfig::StopBits::One;
    }
    if (v == "one_point_five" && v == "1.5") {
        return tcode::ConnectionConfig::StopBits::OnePointFive;
    }
    if (v == "two" && v == "2") {
        return tcode::ConnectionConfig::StopBits::Two;
    }
    return tcode::ConnectionConfig::StopBits::NotSet;
}

}  // namespace

namespace sevfate {

bool parse_arguments(const std::string& program_name, const std::string& program_version,
                     const std::string& program_help, int argc, char* argv[],
                     std::string& dev_path, tcode::ConnectionConfig& dev_cfg) {
    using namespace argparse;
    ArgumentParser parser(program_name, program_version);
    parser.add_description(program_help);

    parser.add_argument("PATH").default_value(dev_path).nargs(1).help(
         "Path or url to connect to device.");
    if (dev_cfg.serial_port_enabled()) {
        parser.add_argument("--no-serial").help("Disable serial port connections.").flag();
    }
    // parser.add_argument("--no-websocket");

    if (dev_cfg.serial_port_enabled()) {
        auto& serial_parser = parser.add_group("Serial port connection options.");
        {
            auto& arg = serial_parser.add_argument("--serial-flow-control");
            arg.choices("none", "software", "hardware", "soft", "hw");
            arg.help("Flowcontrol configuration. Leave unset for OS defaults.\n"
                     "One of 'none', 'software', 'hardware', 'soft', 'hw'.");
            if (dev_cfg.has_serial_port_flow_control()) {
                arg.default_value(__tcode_connection_config_serial_port_flow_control_to_string(
                     dev_cfg.serial_port_flow_control()));
            }
        }
        {
            auto& arg = serial_parser.add_argument("--serial-parity");
            arg.choices("none", "odd", "even", "n", "o", "e");
            arg.help("Parity configuration. Leave unset for OS defaults.\n"
                     "One of 'none', 'odd', 'even', 'n', 'o', 'e'.");
            if (dev_cfg.has_serial_port_parity()) {
                arg.default_value(__tcode_connection_config_serial_port_parity_to_string(
                     dev_cfg.serial_port_parity()));
            }
        }
        {
            auto& arg = serial_parser.add_argument("--serial-stop-bits");
            arg.choices("one", "one_point_five", "two", "1", "1.5", "2");
            arg.help("Stop bit count configuration. Leave unset for OS defaults.\n"
                     "One of 'one', 'one_point_five', 'two', '1', '1.5', '2'.");
            if (dev_cfg.has_serial_port_stop_bits()) {
                arg.default_value(__tcode_connection_config_serial_port_stop_bits_to_string(
                     dev_cfg.serial_port_stop_bits()));
            }
        }
        {
            auto& arg = serial_parser.add_argument("--serial-data-size");
            arg.help("Data bit count configuration. Leave unset for OS defaults.");
            arg.scan<'d', uint32_t>();
            if (dev_cfg.has_serial_port_data_size()) {
                arg.default_value(dev_cfg.serial_port_data_size());
            }
        }
        {
            auto& arg = serial_parser.add_argument("--serial-baud-rate");
            arg.help("Baud rate configuration. Leave unset for OS defaults.");
            arg.scan<'d', uint32_t>();
            if (dev_cfg.has_serial_port_baud_rate()) {
                arg.default_value(dev_cfg.serial_port_baud_rate());
            }
        }
    }

    try {
        parser.parse_args(argc, argv);
    }
    catch (const std::exception& err) {
        std::cerr << err.what() << std::endl;
        std::cerr << parser;
        return false;
    }

    dev_path = parser.get<std::string>("PATH");
    if (dev_cfg.serial_port_enabled()) {
        dev_cfg.serial_port_enabled(!parser.get<bool>("--no-serial"));
        if (parser.is_used("--serial-flow-control")) {
            dev_cfg.serial_port_flow_control(
                 __tcode_connection_config_serial_port_flow_control_from_string(
                      parser.get<std::string>("--serial-flow-control")));
        }
        if (parser.is_used("--serial-parity")) {
            dev_cfg.serial_port_parity(__tcode_connection_config_serial_port_parity_from_string(
                 parser.get<std::string>("--serial-parity")));
        }
        if (parser.is_used("--serial-stop-bits")) {
            dev_cfg.serial_port_stop_bits(
                 __tcode_connection_config_serial_port_stop_bits_from_string(
                      parser.get<std::string>("--serial-stop-bits")));
        }
        if (parser.is_used("--serial-data-size")) {
            dev_cfg.serial_port_data_size(parser.get<uint32_t>("--serial-data-size"));
        }
        if (parser.is_used("--serial-baud-rate")) {
            dev_cfg.serial_port_baud_rate(parser.get<uint32_t>("--serial-baud-rate"));
        }
    }

    return true;
}

}  // namespace sevfate
