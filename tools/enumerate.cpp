#include "common.hpp"
#include "exit_codes.h"

#include <logger/Logger.hpp>
#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>
#include <tcode/ParserDispatcherRegistry.hpp>

#include <ios>
#include <iostream>
#include <string>

static void print_enumeration_info(tcode::ParserDispatcher&, tcode::Registry& reg) {
    std::cout << std::endl;
    std::cout << "Device name: " << reg.get_device_name() << std::endl;
    std::cout << "Device version: " << reg.get_device_version() << std::endl;
    std::cout << "Device uuid:" << std::hex << std::setfill('0');
    // Print binary data as hex.
    for (auto x : reg.get_device_uuid()) {
        std::cout << " " << std::setw(2) << static_cast<uint32_t>(x);
    }
    // Reset formatting options.
    std::cout << std::endl << std::dec << std::setfill(' ') << std::setw(0);
    std::cout << "Protocol name: " << reg.get_protocol_name() << std::endl;
    std::cout << "Protocol version: " << reg.get_protocol_version() << std::endl;

    std::cout << "Min property update interval: " << reg.get_min_update_interval() << std::endl;
    std::cout << "Max property update interval: " << reg.get_max_update_interval() << std::endl;
    std::cout << "Available commands and axis: " << std::endl;
    auto& endpts = reg.get_endpoints();
    for (auto endpt_it = endpts.begin(); endpt_it != endpts.end(); ++endpt_it) {
        std::cout << "- " << std::string_view(endpt_it->first.to_string().data(), 2)
                  << std::endl;
        auto& endpt_mt = endpt_it->second;
        std::cout << "\t"
                  << "Capabilities:";
        if (endpt_mt.supports_direct_call()) {
            std::cout << " callback";
        }
        if (endpt_mt.supports_normal_update()) {
            std::cout << " update";
        }
        if (endpt_mt.supports_interval_update()) {
            std::cout << " interval";
        }
        if (endpt_mt.supports_speed_update()) {
            std::cout << " speed";
        }
        if (endpt_mt.supports_stop_cmd()) {
            std::cout << " stop";
        }
        std::cout << std::endl;
        std::cout << "\t"
                  << "Properties:" << std::endl;
        for (auto& prop : endpt_mt.get_properties()) {
            std::cout << "\t- " << prop.first << std::endl;
            std::cout << "\t\ttype: " << tcode::to_string(prop.second.get_type());
            std::cout << std::endl;
            std::cout << "\t\tflags:";
            if (prop.second.has_flag_read()) {
                std::cout << " read";
            }
            if (prop.second.has_flag_write()) {
                std::cout << " write";
            }
            if (prop.second.has_flag_event()) {
                std::cout << " event";
            }
            if (prop.second.has_flag_action()) {
                std::cout << " action";
            }
            std::cout << std::endl;

            if (prop.second.get_data_interp() !=
                tcode::PropertyMetadata::DataInterpretation::Normal) {
                std::cout << "\t\tdata interpretation: "
                          << tcode::to_string(prop.second.get_data_interp());
                std::cout << std::endl;
            }

            if (prop.second.get_disp_type() != tcode::PropertyMetadata::DisplayType::Default) {
                std::cout << "\t\tdisplay type: "
                          << tcode::to_string(prop.second.get_disp_type());
                std::cout << std::endl;
            }

            // TODO: extra metadata.

            if (prop.second.get_current_update_interval() != 0) {
                std::cout << "\t\tcurrent update interval: "
                          << prop.second.get_current_update_interval() << std::endl;
            }
            if (prop.second.get_suggested_update_interval() != 0) {
                std::cout << "\t\tsuggested update interval: "
                          << prop.second.get_suggested_update_interval() << std::endl;
            }
            std::cout << std::endl;
        }

        std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    tcode::ParserDispatcher tcode_protocol;
    {
        std::string dev_path = "/dev/ttyACM0";
        tcode::ConnectionConfig dev_cfg = {};
        if (!sevfate::parse_arguments("bandwidth", "0.7",
                                      "Measure bandwidth of connection to the device.", argc,
                                      argv, dev_path, dev_cfg)) {
            return TEST_ERROR;
        }
        tcode_protocol.connect(dev_path, dev_cfg);
    }

    if (!tcode_protocol.is_connected()) {
        return TEST_FAIL;
    }

    // tcode_protocol.set_packet_tracing(true);

    tcode_protocol.begin_request();
    tcode_protocol.send_request({tcode::request::CommandType::Device, 0});
    tcode_protocol.send_request({tcode::request::CommandType::Device, 1});
    tcode_protocol.send_request({tcode::request::CommandType::Device, 2});
    tcode_protocol.end_request();

    {
        auto [reg_lck, reg] = tcode_protocol.acquire_registry();
        reg.register_enumeration_complete_callback(
             [&](tcode::ParserDispatcher& obj, tcode::Registry& reg) {
                 // Print stats:
                 print_enumeration_info(obj, reg);
                 // NOTE: This will deadlock only if using a detached thread for IO event loop.
                 obj.disconnect();
             });
    }

    tcode_protocol.start_event_loop();

    return TEST_OK;
}