#include "common.hpp"
#include "exit_codes.h"

#include <logger/Logger.hpp>
#include <tcode/Messages.hpp>
#include <tcode/ParserDispatcher.hpp>

#include <cstdlib>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[]) {
    tcode::ParserDispatcher tcode_protocol;
    {
        std::string dev_path = "/dev/ttyACM0";
        tcode::ConnectionConfig dev_cfg = {};
        if (!sevfate::parse_arguments("bandwidth", "0.7",
                                      "Measure bandwidth of connection to the device.", argc,
                                      argv, dev_path, dev_cfg)) {
            return TEST_ERROR;
        }
        tcode_protocol.connect(dev_path, dev_cfg);
    }

    if (!tcode_protocol.is_connected()) {
        return TEST_FAIL;
    }

    std::atomic<size_t> counter = 0;

    tcode_protocol.register_on_response_end_callback([&](tcode::ParserDispatcher& obj) -> bool {
        ++counter;
        obj.begin_request();
        obj.send_request({tcode::request::CommandType::Device, 0});
        obj.send_request({tcode::request::CommandType::Device, 1});
        obj.send_request({tcode::request::CommandType::Device, 2});
        obj.end_request();
        return true;
    });

    tcode_protocol.start_detached_event_loop([](tcode::ParserDispatcher& obj) {
        obj.begin_request();
        obj.end_request();
    });

    // Keep object 'tcode_protocol' in scope and print stats periodically.
    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        logger.logi(counter.fetch_and(0), " requests/second");
    } while (tcode_protocol.is_connected());

    return TEST_OK;
}